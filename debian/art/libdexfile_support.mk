NAME = libdexfile_support

SOURCES = art/libdexfile/external/dex_file_supp.cc
OBJECTS = $(SOURCES:.cc=.o)

CPPFLAGS += \
  -DNO_DEXFILE_SUPPORT \
  -Iart/libartbase \
  -Iart/libdexfile \
  -Iart/libdexfile/external/include \
  -Isystem/core/liblog/include \
  -Isystem/core/base/include \
  -I/usr/include/android \
  -Umips \

CXXFLAGS += -std=gnu++17

debian/out/art/$(NAME).a: $(OBJECTS)
	mkdir --parents debian/out/art
	ar -rcs $@ $^

$(OBJECTS): %.o: %.cc
	$(CXX) -c -o $@ $< $(CXXFLAGS) $(CPPFLAGS)
