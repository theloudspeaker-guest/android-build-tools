% JAVA-EVENT-LOG-TAGS(1)
% The Android Open Source Project

# NAME

java-event-log-tags - event-logs-tags to Java source translator

# SYNOPSIS

**java-event-log-tags** [-o _output_file_] _input_file_ _merged_tags_file_

# DESCRIPTION

Generates a Java class containing constants for each of the event-log-tags in
the given input file.

# OPTIONS

-h
: Display help.