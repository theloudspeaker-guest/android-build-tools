#!/usr/bin/make -f

include /usr/share/dpkg/architecture.mk
include /usr/share/dpkg/pkg-info.mk

## Security Hardening
export DEB_BUILD_MAINT_OPTIONS = hardening=+all
export DEB_CFLAGS_MAINT_APPEND = -fPIC
export DEB_CPPFLAGS_MAINT_APPEND = -DNDEBUG -UDEBUG
export DEB_CXXFLAGS_MAINT_APPEND = -fPIC
export DEB_LDFLAGS_MAINT_APPEND = -fPIC

## For get-orig-source
export DEB_SOURCE
export DEB_VERSION_UPSTREAM

## build
include /usr/share/javahelper/java-vars.mk

export DEB_HOST_MULTIARCH
export CLASSPATH = /usr/share/java/bcprov.jar:/usr/share/java/bcpkix.jar:/usr/share/java/apksig.jar

## frameworks/base
# https://android.googlesource.com/platform/development/+/android-10.0.0_r36/sdk/build_tools_source.prop_template
export ANDROID_BUILD_TOOLS_VERSION = 29.0.3

## system/core
export DEB_HOST_MULTIARCH
export DEB_VERSION
# https://android.googlesource.com/platform/development/+/refs/tags/android-10.0.0_r36/sdk/plat_tools_source.prop_template
export PLATFORM_TOOLS_VERSION = 28.0.2

ifneq (, $(shell which clang))
  export CC=clang
  export CXX=clang++
endif

COMPONENTS = \
 s/core/libbacktrace.a \
 s/core/libbase.a \
 s/core/libcutils.a \
 s/core/liblog.a \
 s/core/libsparse.a \
 s/core/libutils.a \
 s/core/libziparchive.a \
 s/core/libcrypto_utils.a \
 s/core/simg2img \
 s/core/img2simg \
 s/core/append2simg \
 s/core/simg2simg

COMPONENTS += s/core/adb s/core/libadb.a s/core/fastboot art/libnativebridge.a art/libnativeloader.a

# Whatever depends on BoringSSL must be disabled on MIPS
NON_MIPS_COMPONENTS = s/core/adb s/core/libadb.a s/core/libcrypto_utils.a s/core/fastboot
ifneq ($(filter mips mipsel mips64el,$(DEB_HOST_ARCH)),)
  COMPONENTS := $(filter-out $(NON_MIPS_COMPONENTS),$(COMPONENTS))
endif

# Most components only support ARM, x86 and MIPS, but some can be built
# on all architectures.
COMPONENTS_ANY_ARCH = \
  s/core/append2simg \
  s/core/img2simg \
  s/core/libbase.a \
  s/core/libcutils.a \
  s/core/liblog.a \
  s/core/libsparse.a \
  s/core/simg2img \
  s/core/simg2simg
ifeq ($(filter amd64 i386 armel armhf arm64 mips mipsel mips64el,$(DEB_HOST_ARCH)),)
  COMPONENTS := $(filter $(COMPONENTS_ANY_ARCH), $(COMPONENTS))
endif

## art

# MIPS is left out because ART only supports MIPSr6 while Debian is by default MIPSr2
ifneq ($(filter amd64 i386 armel armhf arm64,$(DEB_HOST_ARCH)),)
  COMPONENTS += art/dexdump art/dexlist
  COMPONENTS += art/libdexfile_external.so art/libdexfile_support.so
endif

## frameworks/native

ifneq ($(filter amd64 i386 armel armhf arm64 mips mipsel mips64el,$(DEB_HOST_ARCH)),)
  COMPONENTS += f/native/libETC1.a
endif

%:
	dh $@ --with maven_repo_helper,javahelper,bash-completion

get-orig-source:
	debian/scripts/get-orig-source

## system/core

s/core/lib%.a: debian/system/core/lib%.mk
	dh_auto_build --buildsystem=makefile -- -f $<

debian/manpages/system/core/%.1: debian/manpages/system/core/%.1.md
	pandoc --standalone --from=markdown-smart --to=man --output=$@ $<

s/core/libadb.a: debian/system/core/libadb.mk s/core/libcutils.a s/core/libbase.a s/core/libcrypto_utils.a e/boringssl/libcrypto.a
	dh_auto_build --buildsystem=makefile -- -file=$<

s/core/libbacktrace.a: debian/system/core/libbacktrace.mk s/core/libcutils.a s/core/libbase.a s/core/liblog.a e/libunwind/libunwind.a art/libdexfile_support.a
	dh_auto_build --buildsystem=makefile -- -file=$<

s/core/libbase.a: debian/system/core/libbase.mk s/core/liblog.a
	dh_auto_build --buildsystem=makefile -- -file=$<

s/core/libcutils.a: debian/system/core/libcutils.mk s/core/liblog.a s/core/libbase.a
	dh_auto_build --buildsystem=makefile -- -file=$<

s/core/libcrypto_utils.a: debian/system/core/libcrypto_utils.mk e/boringssl/libcrypto.a
	dh_auto_build --buildsystem=makefile -- -file=$<

s/core/libutils.a: debian/system/core/libutils.mk s/core/liblog.a s/core/libcutils.a s/core/libbacktrace.a
	dh_auto_build --buildsystem=makefile -- -file=$<

s/core/libziparchive.a: debian/system/core/libziparchive.mk s/core/liblog.a s/core/libbase.a
	dh_auto_build --buildsystem=makefile -- -file=$<

s/core/adb: debian/system/core/adb.mk s/core/libadb.a s/core/libcutils.a s/core/libbase.a debian/manpages/system/core/adb.1
	dh_auto_build --buildsystem=makefile -- -file=$<

s/core/fastboot: debian/system/core/fastboot.mk s/core/libziparchive.a s/core/libsparse.a s/core/libbase.a s/core/libcutils.a s/core/libadb.a e/boringssl/libcrypto.a s/extras/libext4_utils.a debian/manpages/system/core/fastboot.1
	dh_auto_build --buildsystem=makefile -- -file=$<

s/core/simg2img: debian/system/core/simg2img.mk s/core/libbase.a s/core/libsparse.a
	dh_auto_build --buildsystem=makefile -- -file=$<

s/core/simg2simg: debian/system/core/simg2simg.mk s/core/libbase.a s/core/libsparse.a
	dh_auto_build --buildsystem=makefile -- -file=$<

s/core/img2simg: debian/system/core/img2simg.mk s/core/libbase.a s/core/libsparse.a
	dh_auto_build --buildsystem=makefile -- -file=$<

s/core/append2simg: debian/system/core/append2simg.mk s/core/libbase.a s/core/libsparse.a
	dh_auto_build --buildsystem=makefile -- -file=$<

clean_system_core:
	$(RM) debian/manpages/system/core/*.1
	for component in debian/system/core/*.mk; do \
		make clean --file=$$component; \
	done

## build

debian/manpages/build/make/%.1: debian/manpages/build/make/%.1.md
	pandoc --standalone --from=markdown-smart --to=man --output=$@ $<

makeparallel: debian/build/make/makeparallel.mk debian/manpages/build/make/makeparallel.1
	dh_auto_build --buildsystem=makefile -- --file $<

zipalign: debian/build/make/zipalign.mk debian/manpages/build/make/zipalign.1
	dh_auto_build --buildsystem=makefile -- --file $<

ziptime: debian/build/make/ziptime.mk debian/manpages/build/make/ziptime.1
	dh_auto_build --buildsystem=makefile -- --file $<

build/make/signapk.jar: debian/manpages/build/make/signapk.1
	jh_build --javacopts="-encoding UTF-8 -source 1.9 -target 1.9" --no-javadoc --main=com.android.signapk.SignApk $@ build/make/tools/signapk/

build/make/signtos.jar: debian/manpages/build/make/signtos.1
	jh_build --javacopts="-encoding UTF-8 -source 1.9 -target 1.9" --no-javadoc --main=com.android.signtos.SignTos $@ build/make/tools/signtos/

COMPONENTS += makeparallel zipalign ziptime build/make/signapk.jar build/make/signtos.jar debian/manpages/build/make/java-event-log-tags.1 debian/manpages/build/make/merge-event-log-tags.1

## development

debian/manpages/development/%.1: debian/manpages/development/%.1.md
	pandoc --standalone --from=markdown-smart --to=man --output=$@ $<

development/etc1tool: debian/development/etc1tool.mk debian/manpages/development/etc1tool.1 f/native/libETC1.a
	dh_auto_build --buildsystem=makefile -- -file=$<

development/apkcheck: debian/manpages/development/apkcheck.1
	mkdir --parents debian/out/development
	sed 's,^ *libdir=.*,libdir=/usr/share/apkcheck,' development/tools/apkcheck/etc/apkcheck > debian/out/development/apkcheck
	jh_build --no-javadoc --main=com.android.apkcheck.ApkCheck debian/out/development/apkcheck.jar development/tools/apkcheck/src

COMPONENTS += development/etc1tool development/apkcheck

## art

debian/out/art:
	mkdir --parents $@

debian/out/art/%.1: debian/out/art/%
	help2man -N -n "Dex Tool" --no-discard-stderr --version-string="$(DEB_VERSION)" \
		-o $@ $<

art/dmtracedump: debian/art/dmtracedump.mk
	dh_auto_build --buildsystem=makefile -- --file=$<

art/libdexfile_support.a: debian/art/libdexfile_support.mk
	dh_auto_build --buildsystem=makefile -- --file=$<

art/libsigchain.a: debian/art/libsigchain.mk
	dh_auto_build --buildsystem=makefile -- --file=$<

art/dexlist: debian/art/dexlist.mk debian/out/art/libart.so s/core/libbase.a
	dh_auto_build --buildsystem=makefile -- --file=$<

art/dexdump: debian/art/dexdump.mk debian/out/art/libart.so s/core/libbase.a
	dh_auto_build --buildsystem=makefile -- --file=$<

art/libdexfile_external.a: debian/art/libdexfile_external.mk art/libart.a
	dh_auto_build --buildsystem=makefile -- --file=$<

art/libart.a: debian/art/libart.mk debian/out/art/asm_defines.h art/libsigchain.a s/core/libbacktrace.a s/core/libziparchive.a art/libnativeloader.a art/libnativebridge.a s/core/libbase.a
	dh_auto_build --buildsystem=makefile -- --file=$<

art/libnativebridge.a:  debian/art/libnativebridge.mk s/core/liblog.a
	dh_auto_build --buildsystem=makefile -- --file=$<

art/libnativeloader.a: debian/art/libnativeloader.mk art/libnativebridge.a s/core/libbase.a
	dh_auto_build --buildsystem=makefile -- --file=$<


# Only for debug purpose to build standalone object
debian/out/art/asm_defines.h debian/out/art/operator_out.cc: debian/art/libart.mk
	dh_auto_build --buildsystem=makefile -- --file=$< $@
debian/out/art/mterp.S: debian/art/libart.mk debian/out/art debian/out/art/asm_defines.h
	dh_auto_build --buildsystem=makefile -- --file=$< $@
art/runtime/%.a art/libartbase/%.a art/libdexfile/%.a debian/out/art/%.a: debian/art/libart.mk
	dh_auto_build --buildsystem=makefile -- --file=$< $@

COMPONENTS += art/dmtracedump art/dmtracedump.1 art/dexdump.1

## frameworks/native

f/native/libETC1.a: debian/frameworks/native/libETC1.mk
	dh_auto_build --buildsystem=makefile -- -file=$<

## external/boringssl

e/boringssl/libcrypto.a: debian/external/boringssl/libcrypto.mk
	dh_auto_build --buildsystem=makefile -- --file=$<

e/boringssl/libssl.a: debian/external/boringssl/libssl.mk e/boringssl/libcrypto.a
	dh_auto_build --buildsystem=makefile -- --file=$<

COMPONENTS += e/boringssl/libcrypto.a e/boringssl/libssl.a

## external/libunwind

e/libunwind/libunwind.a: debian/external/libunwind/libunwind.mk
	dh_auto_build --buildsystem=makefile -- --file=$<

COMPONENTS += e/libunwind/libunwind.a

## external/selinux

e/selinux/libsepol.a: debian/external/selinux/libsepol.mk
	dh_auto_build --buildsystem=makefile -- --file=$<

e/selinux/libselinux.a: debian/external/selinux/libselinux.mk e/selinux/libsepol.a
	dh_auto_build --buildsystem=makefile -- --file=$<

COMPONENTS += e/selinux/libsepol.a e/selinux/libselinux.a

## frameworks/base

debian/out/frameworks/base:
	mkdir --parents $@

f/base/aapt: debian/frameworks/base/aapt.mk f/base/libaapt.a debian/out/frameworks/base
	dh_auto_build --buildsystem=makefile -- -file=$<

f/base/aapt2: debian/frameworks/base/aapt2.mk f/base/libandroidfw.a debian/out/frameworks/base
	dh_auto_build --buildsystem=makefile -- -file=$<

f/base/aapt2_build-indep: aapt2 debian/out/frameworks/base
	cp /usr/lib/$(DEB_HOST_MULTIARCH)/android/*.so.0 debian/out/frameworks/base
	ln -s /usr/lib/p7zip/7z.so debian/out/frameworks/base/7z.so
	chrpath --replace debian/out/frameworks/base debian/out/frameworks/base/aapt2
	debian/out/frameworks/base/aapt2 compile --dir frameworks/base/core/res/res \
				-o debian/out/frameworks/base/resources.zip
	debian/out/frameworks/base/aapt2 link --manifest frameworks/base/core/res/AndroidManifest.xml \
				debian/out/frameworks/base/resources.zip \
				-o debian/out/frameworks/base/framework-res.apk \
				-A frameworks/base/core/res/assets \
				--min-sdk-version 29 --target-sdk-version 29 \
				--product default \
				--version-code 29 --version-name 10.0.0
	chrpath --replace /usr/lib/$(DEB_HOST_MULTIARCH)/android debian/out/frameworks/base/aapt2

f/base/libaapt.a: debian/frameworks/base/libaapt.mk f/base/libandroidfw.so debian/out/frameworks/base
	dh_auto_build --buildsystem=makefile -- -file=$<

f/base/libandroidfw.a: debian/frameworks/base/libandroidfw.mk debian/out/frameworks/base
	dh_auto_build --buildsystem=makefile -- -file=$<

debian/manpages/frameworks/base/split-select.1: debian/manpages/frameworks/base/split-select.1.md
	pandoc --standalone --output=$@ $<

f/base/split-select: debian/frameworks/base/split-select.mk f/base/libandroidfw.a f/base/libaapt.a debian/out/frameworks/base debian/manpages/frameworks/base/split-select.1
	dh_auto_build --buildsystem=makefile -- -file=$<

COMPONENTS += f/base/aapt f/base/aapt2 f/base/split-select f/base/aapt2_build-indep

## libnativehelper

libnativehelper/libnativehelper.a: debian/libnativehelper/libnativehelper.mk s/core/liblog.a
	dh_auto_build --buildsystem=makefile -- --file=$<

COMPONENTS += libnativehelper/libnativehelper.a

## dalvik
dalvik/hprof-conv: debian/dalvik/hprof-conv.mk
	dh_auto_build --buildsystem=makefile -- --file=$<

COMPONENTS += debian/hprof-conv

override_dh_auto_build-arch: $(COMPONENTS)

override_dh_auto_test:
	echo ignore tests using upstream build system

override_dh_auto_install:
	echo ignore upstream build system install procedure

override_dh_auto_clean:
	dh_auto_clean

override_dh_shlibdeps:
	dh_shlibdeps -l/usr/lib/$(DEB_HOST_MULTIARCH)/android
