NAME = append2simg

SOURCES = system/core/libsparse/append2simg.cpp

CPPFLAGS += \
  -Isystem/core/libsparse/include \
  -Isystem/core/include \
  -Isystem/core/libsparse/include \
  -std=gnu++17 \
  -I/usr/include/android \

LDFLAGS += -lz
STATIC_LIBS = \
  debian/out/system/core/libsparse.a \
  debian/out/system/core/libbase.a \
  debian/out/system/core/liblog.a \

debian/out/system/core/$(NAME): $(SOURCES)
	mkdir --parents debian/out/system/core
	$(CXX) -o $@ $^ $(CPPFLAGS) $(STATIC_LIBS) $(LDFLAGS)
